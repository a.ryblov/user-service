package grpc

import (
	"bytes"
	"context"
	"errors"
	"io"
	"log/slog"
	"net"
	"testing"
	"time"

	"google.golang.org/grpc/credentials/insecure"

	"google.golang.org/grpc"

	"user-service/internal/domain"
	userServiceV1 "user-service/pkg/user-service/api/v1"

	"github.com/stretchr/testify/require"
	"go.uber.org/mock/gomock"
	"google.golang.org/grpc/test/bufconn"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type userServiceFixture struct {
	ctx  context.Context
	buff *bytes.Buffer
	// UserService
	logger *slog.Logger
	app    *MockUserApp
}

func newUserServiceFixture(t *testing.T) *userServiceFixture {
	t.Helper()

	const timeout = 2 * time.Second
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	t.Cleanup(cancel)

	var buff bytes.Buffer
	logger := slog.New(slog.NewTextHandler(&buff, nil))

	ctrl := gomock.NewController(t)
	app := NewMockUserApp(ctrl)

	return &userServiceFixture{
		ctx:    ctx,
		buff:   &buff,
		logger: logger,
		app:    app,
	}
}

func TestUserService_Add(t *testing.T) {
	t.Parallel()

	t.Run("user service add user", func(t *testing.T) {
		t.Parallel()

		const userId = "some_user_id"
		createdAt := time.Time{}
		updatedAt := time.Time{}.Add(time.Hour)

		data := struct {
			attrs        userServiceV1.UserAttrs
			resp         userServiceV1.AddUserResponse
			notification domain.UserNotification
		}{
			attrs: userServiceV1.UserAttrs{
				FirstName: "test_first_name",
				LastName:  "test_last_name",
				NickName:  "test_nick_name",
				Password:  "test_password",
				Email:     "test_email",
				Country:   "test_country",
				CreatedAt: timestamppb.New(time.Time{}),
				UpdatedAt: timestamppb.New(time.Time{}.Add(time.Hour)),
			},
			resp: userServiceV1.AddUserResponse{Id: userId},
			notification: domain.UserNotification{
				UserId:           userId,
				NotificationType: domain.Create,
			},
		}

		userAttrs := domain.UserAttrs{
			FirstName: data.attrs.FirstName,
			LastName:  data.attrs.LastName,
			NickName:  data.attrs.NickName,
			Password:  data.attrs.Password,
			Email:     data.attrs.Email,
			Country:   data.attrs.Country,
			CreatedAt: createdAt,
			UpdatedAt: updatedAt,
		}

		fixture := newUserServiceFixture(t)
		fixture.app.EXPECT().Add(gomock.Any(), userAttrs).Return(userId, nil)
		fixture.app.EXPECT().Notify(gomock.Any(), data.notification).Return(nil)

		userService := NewUserService(fixture.logger, fixture.app)

		req := userServiceV1.AddUserRequest{User: &data.attrs}

		resp, err := userService.Add(fixture.ctx, &req)

		require.Equal(t, resp, &data.resp)
		require.NoError(t, err)

	})
}

func TestUserService_Update(t *testing.T) {
	t.Parallel()

	t.Run("user service update user", func(t *testing.T) {
		t.Parallel()

		const userId = "some_user_id"
		createdAt := time.Time{}
		updatedAt := time.Time{}.Add(time.Hour)

		data := struct {
			attrs        userServiceV1.UserAttrs
			resp         userServiceV1.UpdateUserResponse
			notification domain.UserNotification
		}{
			attrs: userServiceV1.UserAttrs{
				FirstName: "test_first_name",
				LastName:  "test_last_name",
				NickName:  "test_nick_name",
				Password:  "test_password",
				Email:     "test_email",
				Country:   "test_country",
				CreatedAt: timestamppb.New(createdAt),
				UpdatedAt: timestamppb.New(updatedAt),
			},
			resp: userServiceV1.UpdateUserResponse{},
			notification: domain.UserNotification{
				UserId:           userId,
				NotificationType: domain.Update,
			},
		}

		userAttrs := domain.UserAttrs{
			FirstName: data.attrs.FirstName,
			LastName:  data.attrs.LastName,
			NickName:  data.attrs.NickName,
			Password:  data.attrs.Password,
			Email:     data.attrs.Email,
			Country:   data.attrs.Country,
			CreatedAt: createdAt,
			UpdatedAt: updatedAt,
		}

		fixture := newUserServiceFixture(t)
		fixture.app.EXPECT().UpdateUser(gomock.Any(), userId, userAttrs).Return(nil)
		fixture.app.EXPECT().Notify(gomock.Any(), data.notification).Return(nil)

		userService := NewUserService(fixture.logger, fixture.app)

		req := userServiceV1.UpdateUserRequest{
			Id:   userId,
			User: &data.attrs,
		}

		resp, err := userService.Update(fixture.ctx, &req)

		require.Equal(t, resp, &data.resp)
		require.NoError(t, err)

	})
}

func TestUserService_Remove(t *testing.T) {
	t.Parallel()

	t.Run("user service remove user", func(t *testing.T) {
		t.Parallel()

		const userId = "some_user_id"

		data := struct {
			//	attrs        userServiceV1.UserAttrs
			resp         userServiceV1.RemoveUserResponse
			notification domain.UserNotification
		}{
			resp: userServiceV1.RemoveUserResponse{},
			notification: domain.UserNotification{
				UserId:           userId,
				NotificationType: domain.Remove,
			},
		}

		fixture := newUserServiceFixture(t)
		fixture.app.EXPECT().RemoveUser(gomock.Any(), userId).Return(nil)
		fixture.app.EXPECT().Notify(gomock.Any(), data.notification).Return(nil)

		userService := NewUserService(fixture.logger, fixture.app)

		req := userServiceV1.RemoveUserRequest{
			Id: userId,
		}

		resp, err := userService.Remove(fixture.ctx, &req)

		require.Equal(t, resp, &data.resp)
		require.NoError(t, err)

	})
}

func TestUserService_GetUsers(t *testing.T) {
	t.Parallel()

	t.Run("user service get users", func(t *testing.T) {
		t.Parallel()

		now := time.Time{}

		data := struct {
			users []domain.User
		}{
			[]domain.User{
				{
					Id: "user-id-1",
					Attrs: domain.UserAttrs{
						FirstName: "fist-name-1",
						LastName:  "last-name-1",
						NickName:  "nick-name-1",
						Password:  "password-1",
						Email:     "email-1",
						Country:   "country-1",
						CreatedAt: now.Add(time.Minute),
						UpdatedAt: now.Add(time.Hour),
					},
				},
				{
					Id: "user-id-2",
					Attrs: domain.UserAttrs{
						FirstName: "fist-name-2",
						LastName:  "last-name-2",
						NickName:  "nick-name-2",
						Password:  "password-2",
						Email:     "email-2",
						Country:   "country-2",
						CreatedAt: now.Add(2 * time.Minute),
						UpdatedAt: now.Add(2 * time.Hour),
					},
				},
			},
		}

		fixture := newUserServiceFixture(t)
		userCriteria := domain.UserCriteria{
			FirstName: "first-name",
			LastName:  "last-name",
			Limit:     10,
			Offset:    3,
		}
		fixture.app.EXPECT().GetUsers(gomock.Any(), userCriteria).Return(data.users, nil)

		userService := NewUserService(fixture.logger, fixture.app)

		req := userServiceV1.GetUsersRequest{
			FirstName: userCriteria.FirstName,
			LastName:  userCriteria.LastName,
			Limit:     10,
			Offset:    3,
		}

		resp, err := userService.GetUsers(fixture.ctx, &req)

		expected := struct {
			users []*userServiceV1.User
		}{
			users: []*userServiceV1.User{
				{
					Id: "user-id-1",
					Attrs: &userServiceV1.UserAttrs{
						FirstName: "fist-name-1",
						LastName:  "last-name-1",
						NickName:  "nick-name-1",
						Password:  "password-1",
						Email:     "email-1",
						Country:   "country-1",
						CreatedAt: timestamppb.New(now.Add(time.Minute)),
						UpdatedAt: timestamppb.New(now.Add(time.Hour)),
					},
				},
				{
					Id: "user-id-2",
					Attrs: &userServiceV1.UserAttrs{
						FirstName: "fist-name-2",
						LastName:  "last-name-2",
						NickName:  "nick-name-2",
						Password:  "password-2",
						Email:     "email-2",
						Country:   "country-2",
						CreatedAt: timestamppb.New(now.Add(2 * time.Minute)),
						UpdatedAt: timestamppb.New(now.Add(2 * time.Hour)),
					},
				},
			},
		}

		require.EqualValues(t, resp, &userServiceV1.GetUsersResponse{
			Users: expected.users})
		require.NoError(t, err)

	})
}

func server(ctx context.Context, logger *slog.Logger, userService *UserService) (client userServiceV1.UserServiceClient, closer func()) {
	buffer := 10 * 1024 * 1024
	lis := bufconn.Listen(buffer)

	baseServer := grpc.NewServer()
	userServiceV1.RegisterUserServiceServer(baseServer, userService)

	go func() {
		if err := baseServer.Serve(lis); err != nil {
			logger.Error("error serving server", slog.Any("error", err))
		}
	}()

	conn, err := grpc.DialContext(ctx, "",
		grpc.WithContextDialer(func(context.Context, string) (net.Conn, error) {
			return lis.Dial()
		}), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		logger.Error("error connecting to serve", slog.Any("error", err))
	}

	closer = func() {
		err := lis.Close()
		if err != nil {
			logger.Error("error closing listener", slog.Any("error", err))
		}
		baseServer.Stop()
	}

	client = userServiceV1.NewUserServiceClient(conn)

	return client, closer
}

func TestUserService_GetNotifications(t *testing.T) {

	notification1 := domain.UserNotification{
		UserId:           "user-1",
		NotificationType: domain.Create,
	}

	notification2 := domain.UserNotification{
		UserId:           "user-2",
		NotificationType: domain.Remove,
	}

	fixture := newUserServiceFixture(t)

	userService := NewUserService(fixture.logger, fixture.app)

	client, closer := server(fixture.ctx, fixture.logger, userService)
	defer closer()

	notificationCh := make(chan domain.UserNotification, 10)
	fixture.app.EXPECT().ReadNotifications().Return(notificationCh, nil)

	getNotificationReq := userServiceV1.GetNotificationRequest{}
	out, err := client.GetNotifications(fixture.ctx, &getNotificationReq)

	var outs []*userServiceV1.GetNotificationResponse

	notificationCh <- notification1
	notificationCh <- notification2
	close(notificationCh)

	for {
		o, err := out.Recv()
		if errors.Is(err, io.EOF) {
			break
		}
		outs = append(outs, o)
	}

	require.NoError(t, err)
	require.Equal(t, 2, len(outs))
	require.Equal(t, outs[0].GetId(), "user-1")
	require.Equal(t, outs[0].GetNotificationType(), userServiceV1.NotificationType_CREATE)
	require.Equal(t, outs[1].GetId(), "user-2")
	require.Equal(t, outs[1].GetNotificationType(), userServiceV1.NotificationType_REMOVE)
}
