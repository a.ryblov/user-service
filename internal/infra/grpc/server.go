package grpc

import (
	"log"
	"net"

	userServiceV1 "user-service/pkg/user-service/api/v1"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func Start(addr string, userService *UserService) {
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatalf("Failed to listen: %v\n", err)
	}

	defer lis.Close()

	s := grpc.NewServer()
	userServiceV1.RegisterUserServiceServer(s, userService)
	reflection.Register(s)

	defer s.Stop()
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to GRPC serve: %v\n", err)
	}
}
