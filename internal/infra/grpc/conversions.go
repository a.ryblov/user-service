package grpc

import (
	"user-service/internal/domain"
	userServiceV1 "user-service/pkg/user-service/api/v1"

	"google.golang.org/protobuf/types/known/timestamppb"
)

func userToDomain(raw *userServiceV1.UserAttrs) domain.UserAttrs {
	return domain.UserAttrs{
		FirstName: raw.FirstName,
		LastName:  raw.LastName,
		NickName:  raw.NickName,
		Password:  raw.Password,
		Email:     raw.Email,
		Country:   raw.Country,
		CreatedAt: raw.CreatedAt.AsTime(),
		UpdatedAt: raw.UpdatedAt.AsTime(),
	}
}

func userToProto(user *domain.User) *userServiceV1.User {
	return &userServiceV1.User{
		Id: user.Id,
		Attrs: &userServiceV1.UserAttrs{
			FirstName: user.Attrs.FirstName,
			LastName:  user.Attrs.LastName,
			NickName:  user.Attrs.NickName,
			Password:  user.Attrs.Password,
			Email:     user.Attrs.Email,
			Country:   user.Attrs.Country,
			CreatedAt: timestamppb.New(user.Attrs.CreatedAt),
			UpdatedAt: timestamppb.New(user.Attrs.UpdatedAt),
		},
	}
}
