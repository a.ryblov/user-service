package grpc

import (
	"context"

	"user-service/internal/domain"
)

type UserApp interface {
	Add(ctx context.Context, user domain.UserAttrs) (domain.UserId, error)
	GetUsers(ctx context.Context, cr domain.UserCriteria) ([]domain.User, error)
	UpdateUser(ctx context.Context, userId domain.UserId, attrs domain.UserAttrs) error
	RemoveUser(ctx context.Context, userId domain.UserId) error
	Notify(ctx context.Context, notification domain.UserNotification) error
	ReadNotifications() (<-chan domain.UserNotification, error)
}
