package grpc

import (
	"context"
	"log/slog"

	"user-service/internal/domain"
	userServiceV1 "user-service/pkg/user-service/api/v1"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type UserService struct {
	userServiceV1.UserServiceServer

	logger  *slog.Logger
	userApp UserApp
}

func NewUserService(logger *slog.Logger, app UserApp) *UserService {
	return &UserService{
		logger:  logger,
		userApp: app,
	}
}

func (s *UserService) Add(ctx context.Context, request *userServiceV1.AddUserRequest) (*userServiceV1.AddUserResponse, error) {
	s.logger.LogAttrs(
		ctx, slog.LevelInfo, "Add user request", slog.String("request", request.String()),
	)

	user := userToDomain(request.GetUser())

	userId, err := s.userApp.Add(ctx, user)
	if err != nil {
		s.logger.LogAttrs(
			ctx,
			slog.LevelError,
			"Add user request",
			slog.String("err", err.Error()),
			slog.String("request", request.String()),
		)
		return nil, status.Error(codes.Internal, err.Error())
	}

	notification := domain.UserNotification{UserId: userId, NotificationType: domain.Create}
	if err := s.userApp.Notify(ctx, notification); err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &userServiceV1.AddUserResponse{
		Id: userId,
	}, nil
}

func (s *UserService) Update(ctx context.Context, request *userServiceV1.UpdateUserRequest) (*userServiceV1.UpdateUserResponse, error) {
	s.logger.LogAttrs(
		ctx, slog.LevelInfo, "Update user request", slog.String("request", request.String()),
	)

	userId := request.GetId()
	user := userToDomain(request.GetUser())

	err := s.userApp.UpdateUser(ctx, userId, user)
	if err != nil {
		s.logger.LogAttrs(
			ctx,
			slog.LevelError,
			"Update user request",
			slog.String("err", err.Error()),
			slog.String("request", request.String()),
		)
		return nil, status.Error(codes.Internal, err.Error())
	}

	notification := domain.UserNotification{UserId: userId, NotificationType: domain.Update}
	if err := s.userApp.Notify(ctx, notification); err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &userServiceV1.UpdateUserResponse{}, nil
}

func (s *UserService) Remove(ctx context.Context, request *userServiceV1.RemoveUserRequest) (*userServiceV1.RemoveUserResponse, error) {
	s.logger.LogAttrs(
		ctx, slog.LevelInfo, "Update user request", slog.String("request", request.String()),
	)

	userId := request.GetId()
	err := s.userApp.RemoveUser(ctx, userId)
	if err != nil {
		s.logger.LogAttrs(
			ctx,
			slog.LevelError,
			"Remove user request",
			slog.String("err", err.Error()),
			slog.String("request", request.String()),
		)
		return nil, status.Error(codes.Internal, err.Error())
	}

	notification := domain.UserNotification{UserId: userId, NotificationType: domain.Remove}
	if err := s.userApp.Notify(ctx, notification); err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &userServiceV1.RemoveUserResponse{}, nil
}

func (s *UserService) GetUsers(ctx context.Context, request *userServiceV1.GetUsersRequest) (*userServiceV1.GetUsersResponse, error) {

	cr := domain.UserCriteria{
		FirstName: request.FirstName,
		LastName:  request.LastName,
		Limit:     request.Limit,
		Offset:    request.Offset,
	}

	domainUsers, err := s.userApp.GetUsers(ctx, cr)

	if err != nil {
		s.logger.LogAttrs(
			ctx,
			slog.LevelError,
			"Add user request",
			slog.String("err", err.Error()),
			slog.String("request", request.String()),
		)
	}

	var users []*userServiceV1.User
	for _, u := range domainUsers {
		users = append(users, userToProto(&u))
	}

	return &userServiceV1.GetUsersResponse{
		Users: users,
	}, nil
}

func (s *UserService) Version(ctx context.Context, request *userServiceV1.VersionRequest) (*userServiceV1.VersionResponse, error) {
	return &userServiceV1.VersionResponse{
		Version: "1.0",
	}, nil
}

func convert(t domain.NotificationType) userServiceV1.NotificationType {
	switch t {
	case domain.Create:
		return userServiceV1.NotificationType_CREATE
	case domain.Update:
		return userServiceV1.NotificationType_UPDATE
	case domain.Remove:
		return userServiceV1.NotificationType_REMOVE
	}
	return userServiceV1.NotificationType_UNDEFINED
}

func (s *UserService) GetNotifications(request *userServiceV1.GetNotificationRequest, serv userServiceV1.UserService_GetNotificationsServer) error {
	notificationCh, err := s.userApp.ReadNotifications()
	if err != nil {
		s.logger.Error("failed read notifications", slog.Any("error", err))
		return err
	}

	for notification := range notificationCh {
		resp := userServiceV1.GetNotificationResponse{
			Id:               notification.UserId,
			NotificationType: convert(notification.NotificationType),
		}

		s.logger.Info("get notification", slog.Any("notification", notification))

		if err := serv.Send(&resp); err != nil {
			s.logger.Error("failed send notifications", slog.Any("error", err))
			return err
		}
	}

	return nil
}
