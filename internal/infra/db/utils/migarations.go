package utils

import (
	"embed"
	"errors"
	"fmt"
	"log/slog"
	"strings"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/pgx"
	"github.com/golang-migrate/migrate/v4/source/iofs"
)

func MigrateDB(dbParams *DatabaseParameters, migrations embed.FS, path string, logger *slog.Logger) error {
	connString := dbParams.ConnString()
	logger.Info("start migration", slog.String("dbURI", connString))

	source, err := iofs.New(migrations, path)
	if err != nil {
		return fmt.Errorf("migration error: %v", err)
	}
	m, err := migrate.NewWithSourceInstance("iofs", source, strings.Replace(connString, "postgres://", "pgx://", 1))
	if err != nil {
		return err
	}
	defer m.Close()

	if err := m.Up(); err != nil && !errors.Is(err, migrate.ErrNoChange) {
		return err
	}

	return nil
}
