package utils

import (
	"context"
	"log/slog"

	"user-service/internal/infra/utils"

	"github.com/jackc/pgx/v4/pgxpool"
)

func NewPGXPool(ctx context.Context, dbParams DatabaseParameters, logger *slog.Logger) (*pgxpool.Pool, error) {
	connString := dbParams.DbURI()
	conf, err := pgxpool.ParseConfig(connString)
	if err != nil {
		logger.Error("can't parse connection string", slog.String("connString", connString), slog.Any("error", err))
		return nil, err
	}

	conf.ConnConfig.Logger = utils.NewLogger(logger)

	pool, err := pgxpool.ConnectConfig(ctx, conf)
	if err != nil {
		logger.Error("can't connect to db", slog.String("config", conf.ConnString()), slog.Any("error=", err))
		return nil, err
	}

	return pool, nil
}
