package utils

import (
	"fmt"
)

type DatabaseParameters struct {
	User     string
	Password string
	Port     string
	DbName   string
	Host     string
}

func (dbParams DatabaseParameters) DbURI() string {
	return fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable timezone=UTC connect_timeout=5",
		dbParams.Host, dbParams.Port, dbParams.User, dbParams.Password, dbParams.DbName)
}

func (dbParams DatabaseParameters) ConnString() string {
	//dbAddr := fmt.Sprintf("localhost:%s", dbParams.Port)
	dbAddr := fmt.Sprintf("%s:%s", dbParams.Host, dbParams.Port)
	dbURI := fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", dbParams.User, dbParams.Password, dbAddr, dbParams.DbName)
	return dbURI
}
