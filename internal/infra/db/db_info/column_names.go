package db_info

const (
	Id        = "id"
	FirstName = "first_name"
	LastName  = "last_name"
	NickName  = "nick_name"
	Password  = "password"
	Email     = "email"
	Country   = "country"
	CreatedAt = "created_at"
	UpdatedAt = "updated_at"
)
