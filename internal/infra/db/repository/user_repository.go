package repository

import (
	"context"
	"fmt"
	"log/slog"
	"time"

	"user-service/internal/domain"
	c "user-service/internal/infra/db/db_info"

	sq "github.com/Masterminds/squirrel"
	_ "github.com/jackc/pgconn"
	_ "github.com/jackc/pgx/v4"

	"github.com/jackc/pgx/v4/pgxpool"
	_ "github.com/jackc/pgx/v4/stdlib"
	"golang.org/x/crypto/bcrypt"
)

const dbTimeout = time.Second * 3

type UserRepository struct {
	logger *slog.Logger
	ider   IDer
	dbPool *pgxpool.Pool
	sb     sq.StatementBuilderType
}

func NewUserRepository(logger *slog.Logger, dbPool *pgxpool.Pool, ider IDer) (*UserRepository, error) {

	return &UserRepository{
		logger: logger,
		ider:   ider,
		dbPool: dbPool,
		sb:     sq.StatementBuilder.PlaceholderFormat(sq.Dollar),
	}, nil
}

func (s *UserRepository) AddUser(ctx context.Context, attrs domain.UserAttrs) (domain.UserId, error) {

	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	curDate := time.Now()

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(attrs.Password), 12)
	if err != nil {
		return "", err
	}

	sql, args, err := s.sb.Insert(c.Users).
		Columns(c.Id, c.FirstName, c.LastName, c.NickName, c.Password, c.Email, c.Country, c.CreatedAt, c.UpdatedAt).
		Values(s.ider.NewString(), attrs.FirstName, attrs.LastName, attrs.NickName, hashedPassword, attrs.Email, attrs.Country, curDate, curDate).
		Suffix("returning id").
		ToSql()

	var newID string
	if err = s.dbPool.QueryRow(ctx, sql, args...).Scan(&newID); err != nil {
		s.logger.Error("Error creating user", err)
		return "", err
	}

	return newID, nil
}

func (s *UserRepository) UpdateUser(ctx context.Context, userId domain.UserId, attrs domain.UserAttrs) error {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	q := s.sb.Update(c.Users).
		Set(c.FirstName, attrs.FirstName).
		Set(c.LastName, attrs.LastName).
		Set(c.NickName, attrs.NickName).
		Set(c.Password, attrs.Password).
		Set(c.Email, attrs.Email).
		Set(c.Country, attrs.Country).
		Set(c.CreatedAt, attrs.CreatedAt).
		Set(c.UpdatedAt, attrs.UpdatedAt).
		Where(sq.Eq{c.Id: userId})

	sql, args, err := q.ToSql()
	if err != nil {
		return err
	}

	ct, err := s.dbPool.Exec(ctx, sql, args...)
	if ct.RowsAffected() == 0 {
		return fmt.Errorf("user: %v not found", userId)
	}

	return nil
}
func (s *UserRepository) RemoveUser(ctx context.Context, userId string) error {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	q := s.sb.Delete(c.Users).Where(sq.Eq{c.Id: userId})

	sql, args, err := q.ToSql()
	if err != nil {
		return err
	}

	ct, err := s.dbPool.Exec(ctx, sql, args...)

	if ct.RowsAffected() == 0 {
		return fmt.Errorf("user: %v not found", userId)
	}

	return nil
}

func (s *UserRepository) GetUsers(ctx context.Context, cr domain.UserCriteria) ([]domain.User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), dbTimeout)
	defer cancel()

	q := s.sb.Select(c.Id, c.FirstName, c.LastName, c.NickName, c.Password, c.Email, c.Country, c.CreatedAt, c.UpdatedAt).
		From(c.Users)

	if cr.Limit != 0 {
		q = q.Limit(cr.Limit).Offset(cr.Offset)
	}

	if cr.FirstName != "" {
		q = q.Where(sq.Eq{
			c.FirstName: cr.FirstName,
		})
	}

	if cr.LastName != "" {
		q = q.Where(sq.Eq{
			c.LastName: cr.LastName,
		})
	}

	sql, args, err := q.ToSql()
	if err != nil {
		return nil, err
	}

	rows, err := s.dbPool.Query(ctx, sql, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	users := make([]domain.User, 0)
	for rows.Next() {
		var user domain.User
		err = rows.Scan(
			&user.Id,
			&user.Attrs.FirstName,
			&user.Attrs.LastName,
			&user.Attrs.NickName,
			&user.Attrs.Password,
			&user.Attrs.Email,
			&user.Attrs.Country,
			&user.Attrs.CreatedAt,
			&user.Attrs.UpdatedAt,
		)
		if err != nil {
			return nil, err
		}

		users = append(users, user)
	}

	return users, nil
}
