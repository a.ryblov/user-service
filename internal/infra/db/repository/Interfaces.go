package repository

//go:generate mockgen -source interfaces.go -destination mock_test.go -package repository

type IDer interface {
	NewString() string
}
