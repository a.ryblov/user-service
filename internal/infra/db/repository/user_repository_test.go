package repository

import (
	"bytes"
	"context"
	"embed"
	"log/slog"
	"os"
	"testing"
	"time"

	"user-service/internal/domain"
	"user-service/internal/infra/db/utils"

	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/stretchr/testify/require"
	"go.uber.org/mock/gomock"
)

//go:embed sql
var migrations embed.FS

var testDbInstance *pgxpool.Pool

type userRepositoryFixture struct {
	ctx  context.Context
	buff *bytes.Buffer
	// UserRepository dependencies
	logger *slog.Logger
	ider   *MockIDer
}

func TestMain(m *testing.M) {
	testDB := SetupTestDatabase()
	defer testDB.TearDown()

	testDbInstance = testDB.DBInstance

	logger := slog.New(slog.NewTextHandler(os.Stdout, nil))
	err := utils.MigrateDB(testDB.DbParameters, migrations, "sql", logger)
	if err != nil {
		logger.Error("Failed migration", err)
	}

	os.Exit(m.Run())
}

func newUserRepositoryFixture(t *testing.T) *userRepositoryFixture {
	t.Helper()

	const timeout = 2 * time.Second
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	t.Cleanup(cancel)

	var buff bytes.Buffer
	logger := slog.New(slog.NewTextHandler(&buff, nil))

	ctrl := gomock.NewController(t)
	ider := NewMockIDer(ctrl)

	return &userRepositoryFixture{
		ctx:    ctx,
		buff:   &buff,
		logger: logger,
		ider:   ider,
	}
}

func TestUserRepository_AddUser(t *testing.T) {
	t.Parallel()

	t.Run("user repository add new user", func(t *testing.T) {
		t.Parallel()

		data := struct {
			userId domain.UserId
			attrs  domain.UserAttrs
		}{
			userId: "d712fefa-b282-4e1f-9c2f-cdad737b68b4",
			attrs: domain.UserAttrs{
				FirstName: "test_first_name",
				LastName:  "test_last_name",
				NickName:  "test_nick_name",
				Password:  "test_password",
				Email:     "test_email",
				Country:   "test_country",
				CreatedAt: time.Time{},
				UpdatedAt: time.Time{},
			},
		}

		fixture := newUserRepositoryFixture(t)
		userRepository, err := NewUserRepository(
			fixture.logger,
			testDbInstance,
			fixture.ider,
		)
		fixture.ider.EXPECT().NewString().Return(data.userId)

		userId, err := userRepository.AddUser(fixture.ctx, data.attrs)

		require.Equal(t, data.userId, userId)
		require.NoError(t, err)
	})
}
