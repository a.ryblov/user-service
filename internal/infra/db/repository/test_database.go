package repository

import (
	"context"
	"fmt"
	"log"
	"time"

	"user-service/internal/infra/db/utils"

	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/modules/postgres"
	"github.com/testcontainers/testcontainers-go/wait"
)

const (
	dbName = "postgres"
	dbUser = "postgres"
	dbPass = "password"
	dbPort = "5432"
)

type TestDatabase struct {
	DBInstance   *pgxpool.Pool
	DbParameters *utils.DatabaseParameters
	container    testcontainers.Container
}

func (tdb *TestDatabase) TearDown() {
	tdb.DBInstance.Close()
	_ = tdb.container.Terminate(context.Background())
}

func createContainer(ctx context.Context) (testcontainers.Container, *pgxpool.Pool, *utils.DatabaseParameters, error) {

	container, err := postgres.RunContainer(ctx,
		testcontainers.WithImage("postgres:14.0"),
		postgres.WithDatabase(dbName),
		postgres.WithUsername(dbUser),
		postgres.WithPassword(dbPass),
		testcontainers.WithWaitStrategy(
			wait.ForLog("database system is ready to accept connections").
				WithOccurrence(2).
				WithStartupTimeout(5*time.Second)),
	)

	if err != nil {
		return nil, nil, nil, fmt.Errorf("failed to create container: %v", err)
	}

	connStr, err := container.ConnectionString(ctx, "sslmode=disable")
	if err != nil {
		return nil, nil, nil, fmt.Errorf("failed to create container (connection string): %v", err)
	}

	exposedPort, err := container.MappedPort(ctx, dbPort)
	if err != nil {
		return nil, nil, nil, fmt.Errorf("failed to create container (mapped port): %v", err)
	}

	time.Sleep(time.Second)

	db, err := pgxpool.Connect(ctx, connStr)
	if err != nil {
		return container, db, nil, fmt.Errorf("failed to establish database connection: %v", err)
	}

	dbParams := utils.DatabaseParameters{
		User:     dbUser,
		Password: dbPass,
		Port:     exposedPort.Port(),
		DbName:   dbName,
		Host:     "localhost",
	}

	return container, db, &dbParams, nil
}

func SetupTestDatabase() *TestDatabase {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*60)
	container, dbInstance, dbParams, err := createContainer(ctx)
	if err != nil {
		log.Fatal("Failed to create container", err)
	}
	cancel()

	return &TestDatabase{
		DBInstance:   dbInstance,
		DbParameters: dbParams,
		container:    container,
	}
}
