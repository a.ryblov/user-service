package kafka_utils

import "github.com/IBM/sarama"

func CreateConsumerGroup(params KafkaParameters) (sarama.ConsumerGroup, error) {
	config := sarama.NewConfig()
	config.Consumer.Offsets.Initial = sarama.OffsetOldest
	var consumerGroup, err = sarama.NewConsumerGroup([]string{params.Addr()}, params.GroupId, config)
	return consumerGroup, err
}
