package kafka_utils

import (
	"fmt"
)

const UserNotificationTopic = "users-notification"

type KafkaParameters struct {
	Host    string
	Port    string
	Topic   string
	GroupId string
}

func (p KafkaParameters) Addr() string {
	return fmt.Sprintf("%s:%s", p.Host, p.Port)
}
