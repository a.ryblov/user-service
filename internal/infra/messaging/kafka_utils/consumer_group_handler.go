package kafka_utils

import (
	"encoding/json"
	"github.com/IBM/sarama"
	"log/slog"
	"user-service/internal/domain"
)

type ConsumerGroupHandler struct {
	logger         *slog.Logger
	Ready          chan bool
	notificationCh chan domain.UserNotification
}

func (consumer *ConsumerGroupHandler) Setup(sarama.ConsumerGroupSession) error {
	close(consumer.Ready)
	return nil
}

func (consumer *ConsumerGroupHandler) Cleanup(sarama.ConsumerGroupSession) error {
	return nil
}

func (consumer *ConsumerGroupHandler) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	for {
		select {
		case message, ok := <-claim.Messages():
			if !ok {
				consumer.logger.Info("message channel was closed")
				return nil
			}
			var notification domain.UserNotification
			err := json.Unmarshal(message.Value, &notification)
			if err != nil {
				consumer.logger.Error("can't parse notification", slog.Any("error", err))
				return err
			}
			consumer.notificationCh <- notification
		case <-session.Context().Done():
			return nil
		}
	}
}

func NewConsumerGroupHandler(logger *slog.Logger, notificationCh chan domain.UserNotification) *ConsumerGroupHandler {
	return &ConsumerGroupHandler{
		logger:         logger,
		Ready:          make(chan bool),
		notificationCh: notificationCh,
	}
}
