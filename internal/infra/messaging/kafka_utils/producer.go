package kafka_utils

import (
	"github.com/IBM/sarama"
)

func CreateProducer(params KafkaParameters) (sarama.SyncProducer, error) {
	config := sarama.NewConfig()
	config.Producer.Return.Successes = true
	config.Producer.Return.Errors = true

	producer, err := sarama.NewSyncProducer([]string{params.Addr()}, config)
	if err != nil {
		return nil, err
	}

	return producer, nil
}
