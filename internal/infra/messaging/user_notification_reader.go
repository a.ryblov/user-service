package messaging

import (
	"context"
	"errors"
	"log"
	"log/slog"
	"user-service/internal/infra/messaging/kafka_utils"

	"user-service/internal/domain"

	"github.com/IBM/sarama"
)

type UserNotificationReader struct {
	logger        *slog.Logger
	topic         string
	consumerGroup *sarama.ConsumerGroup
}

func NewUserNotificationReader(logger *slog.Logger, topic string, consumerGroup *sarama.ConsumerGroup) *UserNotificationReader {
	return &UserNotificationReader{
		logger:        logger,
		topic:         topic,
		consumerGroup: consumerGroup,
	}
}

func (s *UserNotificationReader) GetNotifications() (<-chan domain.UserNotification, error) {
	notificationCh := make(chan domain.UserNotification)

	ctx := context.Background()
	consumer := kafka_utils.NewConsumerGroupHandler(s.logger, notificationCh)

	go func() {
		for {
			if err := (*s.consumerGroup).Consume(ctx, []string{s.topic}, consumer); err != nil {
				if errors.Is(err, sarama.ErrClosedConsumerGroup) {
					return
				}
				log.Panicf("Error from consumer: %v", err)
			}
			// check if context was cancelled, signaling that the consumer should stop
			if ctx.Err() != nil {
				return
			}
		}
	}()

	<-consumer.Ready

	return notificationCh, nil
}
