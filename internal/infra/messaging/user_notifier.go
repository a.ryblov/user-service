package messaging

import (
	"encoding/json"
	"log"
	"log/slog"

	"user-service/internal/domain"

	"github.com/IBM/sarama"
)

type UserNotifier struct {
	logger   *slog.Logger
	topic    string
	producer *sarama.SyncProducer
}

func NewUserNotifier(logger *slog.Logger, topic string, producer *sarama.SyncProducer) *UserNotifier {
	return &UserNotifier{
		logger:   logger,
		topic:    topic,
		producer: producer,
	}
}

func (s *UserNotifier) Notify(notification domain.UserNotification) error {
	// TODO change encoder
	msg := notification.UserId
	s.logger.Info("notify", slog.String("message", msg))

	messageBytes, err := json.Marshal(notification)
	if err != nil {
		s.logger.Error("failed encode notifications", slog.Any("error", err))
		return err
	}
	message := &sarama.ProducerMessage{Topic: s.topic, Key: sarama.StringEncoder("1"), Value: sarama.ByteEncoder(messageBytes)}

	s.logger.Info("send message", slog.Any("message", message))

	status := (*s.producer).TxnStatus()
	slog.Info("status", slog.Any("s", status))

	partition, offset, err := (*s.producer).SendMessage(message)
	if err != nil {
		s.logger.Info("SendMessage err: ", slog.Any("error", err))
		return err
	}

	log.Printf("[producer] partition id: %d; offset:%d, value: %s\n", partition, offset, msg)
	return nil
}
