package utils

import (
	"context"
	"fmt"

	userServiceV1 "user-service/pkg/user-service/api/v1"

	"google.golang.org/grpc"
)

type StreamMock struct {
	grpc.ServerStream
	ctx            context.Context
	sentFromServer chan *userServiceV1.GetNotificationResponse
}

func NewStreamMock() *StreamMock {
	return &StreamMock{
		ctx:            context.Background(),
		sentFromServer: make(chan *userServiceV1.GetNotificationResponse, 10),
	}
}
func (m *StreamMock) Send(resp *userServiceV1.GetNotificationResponse) error {
	m.sentFromServer <- resp
	return nil
}

func (m *StreamMock) ResponsesFromSerer() (*userServiceV1.GetNotificationResponse, error) {
	resp, ok := <-m.sentFromServer
	if !ok {
		return nil, fmt.Errorf("stream is empty")
	}
	return resp, nil
}

func (m *StreamMock) Close() {
	close(m.sentFromServer)
}
