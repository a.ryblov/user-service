package domain

type NotificationType int64

const (
	Undefined NotificationType = iota
	Create
	Update
	Remove
)

func (t NotificationType) String() string {
	switch t {
	case Create:
		return "create"
	case Update:
		return "update"
	case Remove:
		return "remove"
	}
	return "unknown"
}
