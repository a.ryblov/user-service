package domain

import (
	"time"
)

type UserAttrs struct {
	FirstName string
	LastName  string
	NickName  string
	Password  string
	Email     string
	Country   string
	CreatedAt time.Time
	UpdatedAt time.Time
}

type UserId = string

type User struct {
	Id    UserId
	Attrs UserAttrs
}

type UserCriteria struct {
	FirstName string
	LastName  string
	Limit     uint64
	Offset    uint64
}

type UserNotification struct {
	UserId           UserId           `json:"id"`
	NotificationType NotificationType `json:"type"`
}
