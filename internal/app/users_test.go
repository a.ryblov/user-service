package app

import (
	"bytes"
	"context"
	"log/slog"
	"testing"
	"time"

	"user-service/internal/domain"

	"github.com/stretchr/testify/require"
	"go.uber.org/mock/gomock"
)

type usersFixture struct {
	ctx                context.Context
	buff               *bytes.Buffer
	logger             *slog.Logger
	store              *MockUserStore
	notifier           *MockUserNotifier
	notificationReader *MockUserNotificationReader
}

func newUsersFixture(t *testing.T) *usersFixture {
	t.Helper()

	const timeout = 2 * time.Second
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	t.Cleanup(cancel)

	ctrl := gomock.NewController(t)
	store := NewMockUserStore(ctrl)
	notifier := NewMockUserNotifier(ctrl)
	notificationReader := NewMockUserNotificationReader(ctrl)

	var buff bytes.Buffer
	logger := slog.New(slog.NewTextHandler(&buff, nil))

	return &usersFixture{
		ctx:                ctx,
		buff:               &buff,
		logger:             logger,
		store:              store,
		notifier:           notifier,
		notificationReader: notificationReader,
	}

}

func TestUsers_Add(t *testing.T) {
	t.Parallel()

	t.Run("user app add the new user", func(t *testing.T) {
		t.Parallel()

		data := struct {
			userId string
			attrs  domain.UserAttrs
		}{
			userId: "test_user_id",
			attrs: domain.UserAttrs{
				FirstName: "test_first_name",
				LastName:  "test_last_name",
				NickName:  "test_nick_name",
				Password:  "test_password",
				Email:     "test_email",
				Country:   "test_country",
				CreatedAt: time.Time{},
				UpdatedAt: time.Time{},
			},
		}

		fixture := newUsersFixture(t)
		usersApp := New(fixture.logger, fixture.store, fixture.notifier, fixture.notificationReader)
		fixture.store.EXPECT().
			AddUser(gomock.Any(), data.attrs).
			Return(data.userId, nil)

		userId, err := usersApp.Add(
			fixture.ctx,
			data.attrs,
		)

		require.NoError(t, err)
		require.Equal(t, userId, data.userId)
	})
}
