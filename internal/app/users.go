package app

import (
	"context"
	"fmt"
	"log/slog"

	"user-service/internal/domain"
)

type Users struct {
	logger             *slog.Logger
	store              UserStore
	notifier           UserNotifier
	notificationReader UserNotificationReader
}

func New(logger *slog.Logger, store UserStore, notifier UserNotifier, notificationReader UserNotificationReader) *Users {
	return &Users{logger: logger, store: store, notifier: notifier, notificationReader: notificationReader}
}

func (u *Users) Update(ctx context.Context, user domain.UserAttrs) (domain.UserId, error) {
	userId, err := u.store.AddUser(ctx, user)

	if err != nil {
		return "", fmt.Errorf("adding user: %v to store: %v", user, err)
	}

	return userId, nil
}

func (u *Users) Add(ctx context.Context, attrs domain.UserAttrs) (domain.UserId, error) {
	userId, err := u.store.AddUser(ctx, attrs)
	if err != nil {
		return "", fmt.Errorf("add user: %v to store: %v", attrs, err)
	}

	return userId, nil
}

func (u *Users) GetUsers(ctx context.Context, cr domain.UserCriteria) ([]domain.User, error) {
	users, err := u.store.GetUsers(ctx, cr)

	if err != nil {
		return nil, fmt.Errorf("get users: %v from store: %v", cr, err)
	}

	return users, nil
}

func (u *Users) UpdateUser(ctx context.Context, userId domain.UserId, attrs domain.UserAttrs) error {
	if err := u.store.UpdateUser(ctx, userId, attrs); err != nil {
		return fmt.Errorf("update user: %v: %v", userId, err)
	}
	return nil
}

func (u *Users) RemoveUser(ctx context.Context, userId domain.UserId) error {
	if err := u.store.RemoveUser(ctx, userId); err != nil {
		return fmt.Errorf("remove user: %v: %v", userId, err)
	}
	return nil
}

func (u *Users) Notify(ctx context.Context, notification domain.UserNotification) error {
	if err := u.notifier.Notify(notification); err != nil {
		u.logger.LogAttrs(
			ctx,
			slog.LevelError,
			"Send notification",
			slog.String("err", err.Error()),
			slog.String("userId", notification.UserId),
			slog.String("type", notification.NotificationType.String()),
		)
		return fmt.Errorf("send notification user: %v: %v", notification, err)
	}
	return nil
}

func (u *Users) ReadNotifications() (<-chan domain.UserNotification, error) {
	notificationCh, err := u.notificationReader.GetNotifications()
	if err != nil {
		return nil, err
	}
	return notificationCh, nil
}
