package app

import (
	"context"

	"user-service/internal/domain"
)

//go:generate mockgen -source interfaces.go -destination mock_test.go -package app

type UserStore interface {
	AddUser(ctx context.Context, attrs domain.UserAttrs) (domain.UserId, error)
	UpdateUser(ctx context.Context, userId domain.UserId, attrs domain.UserAttrs) error
	RemoveUser(ctx context.Context, userId string) error
	GetUsers(ctx context.Context, cr domain.UserCriteria) ([]domain.User, error)
}

type UserNotifier interface {
	Notify(notification domain.UserNotification) error
}

type UserNotificationReader interface {
	GetNotifications() (<-chan domain.UserNotification, error)
}
