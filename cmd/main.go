package main

import (
	"context"
	"embed"
	"log/slog"
	"os"

	"user-service/internal/app"
	"user-service/internal/infra/db/repository"
	"user-service/internal/infra/db/utils"
	"user-service/internal/infra/grpc"
	notifier "user-service/internal/infra/messaging"
	"user-service/internal/infra/messaging/kafka_utils"

	"github.com/google/uuid"
)

//go:embed sql
var migrations embed.FS

func readDBParameters() utils.DatabaseParameters {
	return utils.DatabaseParameters{
		User:     os.Getenv("POSTGRES_USER"),
		Password: os.Getenv("POSTGRES_PASSWORD"),
		Port:     os.Getenv("POSTGRES_PORT"),
		DbName:   os.Getenv("POSTGRES_DB"),
		Host:     os.Getenv("POSTGRES_DB"),
	}
}

func readKafkaParameter() kafka_utils.KafkaParameters {
	return kafka_utils.KafkaParameters{
		Host:    "kafka",
		Port:    "9092",
		GroupId: "user-notifications-1",
	}
}

type googleUUID struct{}

func (u *googleUUID) NewString() string {
	return uuid.NewString()
}

func main() {

	logger := slog.New(slog.NewTextHandler(os.Stdout, nil))
	logger.Info("Starting user service")

	ctx := context.Background()

	dbParameters := readDBParameters()
	logger.Info("start connect to db", slog.Any("dbParameters", dbParameters))
	dbPool, err := utils.NewPGXPool(ctx, dbParameters, logger)
	if err != nil {
		logger.Error("cannot set pgx pool", slog.Any("dbParameters", dbParameters), slog.Any("error", err))
		panic(err)
	}
	defer dbPool.Close()

	//migration
	if err := utils.MigrateDB(&dbParameters, migrations, "sql", logger); err != nil {
		logger.Error("Migration error", slog.Any("error", err))
		panic(err)
	}
	logger.Info("end migration")

	repo, err := repository.NewUserRepository(logger, dbPool, &googleUUID{})
	if err != nil {
		panic(err)
	}

	kafkaParameters := readKafkaParameter()
	logger.Info("Start connect to Kafka", slog.Any("parameters", kafkaParameters))
	producer, err := kafka_utils.CreateProducer(kafkaParameters)
	if err != nil {
		logger.Error("Connect to Kafka error", slog.Any("error", err))
		panic(err)
	}
	logger.Info("Success connect to Kafka", slog.Any("producer", producer))
	defer producer.Close()

	userNotifier := notifier.NewUserNotifier(logger, kafka_utils.UserNotificationTopic, &producer)
	if err != nil {
		panic(err)
	}

	consumerGroup, err := kafka_utils.CreateConsumerGroup(kafkaParameters)
	if err != nil {
		logger.Error("Connect to Kafka error", slog.Any("error", err))
		panic(err)
	}
	defer consumerGroup.Close()

	userNotificationReader := notifier.NewUserNotificationReader(logger, kafka_utils.UserNotificationTopic, &consumerGroup)

	userApp := app.New(logger, repo, userNotifier, userNotificationReader)

	userService := grpc.NewUserService(logger, userApp)

	addr := "0.0.0.0:50051"
	grpc.Start(addr, userService)
}
