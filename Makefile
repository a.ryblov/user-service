APP_NAME = user-service-api
APP_BINARY = bin/user-service-api

GOENV = CGO_ENABLED=0 GOOS=linux GOARCH=amd64

## build the binary
.PHONY: build
build:
	@echo "building binary user service"
	$(GOENV) go build -o ${APP_BINARY} ./cmd
	@echo "building binary user service done"
test:
	go test ./...
start: build ## build and start docker containers
	@echo "building docker images"
	$ docker-compose build
	$ docker-compose up