# user-service

[task description](TASK.md)

### How to start
- build app binary

  ```
  make build
  ```
  
- build and start docker images
  ```
  make start
  ```  

### How to test

Add a new user
```
grpcurl --plaintext  -d '{"user": {"first_name": "Johnny", "last_name": "Depp", "nick_name": "sparrow", "password": "secret-password", "email": "John1963@gmail.com", "country": "USA"}}' 0.0.0.0:50051 UserService/Add
```
Update user
```
grpcurl --plaintext  -d '{"id": "0df9823b-b6cc-4f18-a265-1494fd859b64", "user": {"password": "secret-password-2"}}' 0.0.0.0:50051 UserService/Update
```
Remove user
```
grpcurl --plaintext  -d '{"id": "0df9823b-b6cc-4f18-a265-1494fd859b64"}' 0.0.0.0:50051 UserService/Remove
```
Get list of users
```
grpcurl --plaintext  -d '{"first_name": "Johnny"}' 0.0.0.0:50051 UserService/GetUser
```

get notifications
```
grpcurl --plaintext  -d '{}' 0.0.0.0:50051 UserService/GetNotifications
```

### Explanation of the choices
- GRPC (better performance and reduced latency, smaller payloads)
- Kafka (high performance pull based message queue)
- PostgreSQL (open source relation database, good for work with entity with predefined schema)
- log/slog (modern structured logging library)

### Possible extensions
- Add REST API
- Add a health check
- Add ability to read notifications for many clients (pass consumer group id as parameter)
- Increase test coverage