FROM alpine:latest

RUN mkdir /app

COPY bin/user-service-api /app

CMD ["/app/user-service-api"]

EXPOSE 50051/tcp